/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.struct;

import java.util.List;

/**
A superset of List and ReadOnlyList.
*/
public interface SemiMutableList<Type> extends List<Type>, ReadOnlyList<Type>, SemiMutableCollection<Type>
{




}


















