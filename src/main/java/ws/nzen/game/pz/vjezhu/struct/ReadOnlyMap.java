/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.struct;

/**

*/
public interface ReadOnlyMap<Key, Value> extends ReadOnlyCollection<Key>
{


	public ReadOnlySet<Key> allKeys();


	public ReadOnlyCollection<Value> allValues();


	public boolean containsKey( Key unknown );


	public Value getVal( Key unknown );


	public Value getOrFallback( Key unknown, Value fallback );


	public boolean hasVal( Value unknown );


}


















