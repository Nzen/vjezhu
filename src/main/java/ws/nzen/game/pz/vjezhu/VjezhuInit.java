
package ws.nzen.game.pz.vjezhu;

import java.awt.EventQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ws.nzen.game.pz.vjezhu.model.PuzzleBoard;
import ws.nzen.game.pz.vjezhu.model.Selection;
import ws.nzen.game.pz.vjezhu.op.LoadsPuzzle;

@SpringBootApplication
public class VjezhuInit  implements CommandLineRunner
{

	private static final Logger log = LoggerFactory.getLogger( VjezhuInit.class );
	private PuzzleBoard board;
	// private VjezhuFrame gui;
	private Selection cursor;


	public void run(
			String... input
	) {
		// just do whatever
	}


	public static void main(
			String args[]
	) {
		 springBootBegin( args );
	}


	private static void springBootBegin(
			String[] args
	) {
		SpringApplication.run( VjezhuInit.class, args );
	}


	public VjezhuInit(
	) {
		init();
	}


	public VjezhuInit(
			boolean ignored_but_means_spring_boot
	) {
		
	}


	private void init(
	) {
		board = new PuzzleBoard();
		new LoadsPuzzle().fillWithHandCopiedPuzzle( board );

		cursor = new Selection( board.getCells().keySet().iterator().next() ); // ¶ any cell will do
		cursor.setRegion( Selection.CellElement.COLUMN_COMBINATIONS ); // 4tests
		cursor.setCombinationIndex( 0 ); // 4tests
	}



}


































