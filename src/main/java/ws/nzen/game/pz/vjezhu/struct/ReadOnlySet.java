/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.struct;

/**
A read only collection, with the expectation that its implementation preserves Set constraints.
*/
public interface ReadOnlySet<Type> extends ReadOnlyCollection<Type>
{



}


















