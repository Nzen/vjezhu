/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.adapt;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ws.nzen.game.pz.vjezhu.model.PuzzleBoard;
import ws.nzen.game.pz.vjezhu.model.readonly.Board;
import ws.nzen.game.pz.vjezhu.op.LoadsPuzzle;

@Configuration
/**

*/
public class Factory
{
	@Bean
	public static Board emptyPuzzleBoard(
	) {
		PuzzleBoard board = new PuzzleBoard();
		new LoadsPuzzle().fillWithHandCopiedPuzzle( board );
		return board;
	}

}


















