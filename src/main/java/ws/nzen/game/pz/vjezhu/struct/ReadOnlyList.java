/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.struct;

/**
A list that only exposes get().
*/
public interface ReadOnlyList<Type> extends ReadOnlyCollection<Type>
{


	Type getVal( int index );


	int index( Type unknown );


	int lastIndex( Type unknown );


}


















