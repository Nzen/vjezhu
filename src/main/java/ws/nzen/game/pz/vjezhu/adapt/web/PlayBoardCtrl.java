/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.adapt.web;

import java.util.Map;
import java.util.TreeMap;
/*
import static j2html.TagCreator.*;
import j2html.attributes.Attr;
import j2html.tags.Tag;
import j2html.tags.UnescapedText;
*/
import org.apache.commons.lang3.tuple.MutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import ws.nzen.game.pz.vjezhu.model.BoardCoordinate;
import ws.nzen.game.pz.vjezhu.model.readonly.Board;
import ws.nzen.game.pz.vjezhu.model.readonly.Cell;
import ws.nzen.game.pz.vjezhu.model.readonly.Coordinate;
import ws.nzen.game.pz.vjezhu.model.readonly.Group;
import ws.nzen.game.pz.vjezhu.struct.ReadOnlyMap;

@Controller
/**  */
public class PlayBoardCtrl
{
	private static final Logger log = LoggerFactory.getLogger( PlayBoardCtrl.class );
	@Autowired
	private Board board;


	@GetMapping( "/easyboard" )
	public String someBoard(
			Model model
	) {
		return "base_page";
	}


	@GetMapping( value = "/addInput/{componentId}", produces="text/html" )
	public String more_input(
			Model model, @PathVariable String componentId
	) {
		model.addAttribute( "id_of_input", componentId );
		return "add_input";
	}


	@GetMapping( value = "/entire_board", produces="text/html" )
	public String actualBoard(
			Model model
	) {
		ReadOnlyMap<Coordinate, ? extends Cell> cells = board.getCells();

		// get boundaries
		int maxXx = 0, maxYy = 0;
		for ( Coordinate coordinate : cells.allKeys() )
		{
			if ( coordinate.getXxPosition() > maxXx )
				maxXx = coordinate.getXxPosition();
			if ( coordinate.getYyPosition() > maxYy )
				maxYy = coordinate.getYyPosition();
		}

		// gen clue cells
		Map<Coordinate, MutablePair<Integer, Integer>> row_columnClues = new TreeMap<>();
		BoardCoordinate candidate = new BoardCoordinate( maxXx, maxYy );
		Coordinate probe;
		for ( Group row : board.getRows() )
		{
			probe = row.getCells().getVal( 0 );
			BoardCoordinate rowCandidate = new BoardCoordinate(
					probe.getXxPosition() -1, probe.getYyPosition() );
			if ( ! row_columnClues.containsKey( rowCandidate ) )
			{
				MutablePair<Integer, Integer> row_column = new MutablePair<>();
				row_columnClues.put( rowCandidate, row_column );
			}
			MutablePair<Integer, Integer> row_column = row_columnClues.get( rowCandidate );
			row_column.setLeft( row.getSumOfCellValues() );
		}
		for ( Group column : board.getColumns() )
		{
			probe = column.getCells().getVal( 0 );
			BoardCoordinate columnCandidate = new BoardCoordinate(
					probe.getXxPosition(), probe.getYyPosition() -1 );
			if ( ! row_columnClues.containsKey( columnCandidate ) )
			{
				MutablePair<Integer, Integer> row_column = new MutablePair<>();
				row_columnClues.put( columnCandidate, row_column );
			}
			MutablePair<Integer, Integer> row_column = row_columnClues.get( columnCandidate );
			row_column.setRight( column.getSumOfCellValues() );
		}

		// gen html
		StringBuilder entireBoardHtml = new StringBuilder( 1_000 );
		entireBoardHtml.append( "\t<table>\r\n<tbody>" );

		for ( int currYy = 0; currYy <= maxYy; currYy += 1 )
		{
			entireBoardHtml.append( "					<tr>\r\n" );
			for ( int currXx = 0; currXx <= maxXx; currXx += 1 )
			{
				candidate.setXxPosition( currXx );
				candidate.setYyPosition( currYy );
				if ( row_columnClues.containsKey( candidate ) )
				{
					entireBoardHtml.append( "						<td\r\n"
							+ "								class = \"td_clue\">\r\n" );
					MutablePair<Integer, Integer> bothClues = row_columnClues.get( candidate );
					if ( bothClues == null )
						entireBoardHtml.append( " " );
					else if ( bothClues.left != null && bothClues.right != null )
						entireBoardHtml.append( bothClues.getRight() ).append( "\\" ).append( bothClues.getLeft() );
					else if ( bothClues.left != null )
						entireBoardHtml.append( "\\" ).append( bothClues.getLeft() ); // divider
					else
						entireBoardHtml.append( bothClues.getRight() ).append( "\\" ); // divider
					entireBoardHtml.append( "\r\n" );
						
							entireBoardHtml.append( "							</td>\r\n" );
				}
				else if ( cells.containsKey( candidate ) )
				{
					String idOfRow = "r"+ candidate.getXxPosition() +"_"+ candidate.getYyPosition() +"_0";
					String idOfColumn = "c"+ candidate.getXxPosition() +"_"+ candidate.getYyPosition() +"_0";
					entireBoardHtml.append( "						<td>\r\n"
							+ "							\r\n"
							+ "							<table>\r\n"
							+ "								<tbody>\r\n"
							+ "									<tr>\r\n"
							+ "										<td>\r\n"
							+ "											<table>\r\n"
							+ "												<tbody>\r\n"
							+ "													<tr>\r\n"
							+ "														<td>\r\n"
							+ "															<input\r\n"
							+ "																	type = \"text\"\r\n"
							+ "																	value = \"\"\r\n"
							+ "																	size = \"10\"\r\n"
							+ "																/>\r\n"
							+ "															<div id = \""+ idOfRow +"\">\r\n"
							+ "																<br />\r\n"
							+ "																<button "
									+ "hx-get = \"/addInput/"+ idOfRow +"\""
											+ "hx-target = \"#"+ idOfRow +"\""
													+ "hx-swap = \"outerHTML\""
													+ ">\r\n"
							+ "																	+\r\n"
							+ "																	</button>\r\n"
							+ "																</div>\r\n"
							+ "															</td>\r\n"
							+ "														</tr>\r\n"
							+ "													</tbody>\r\n"
							+ "												</table>\r\n"
							+ "											</td>\r\n"
							+ "										<td>\r\n"
							+ "											<table>\r\n"
							+ "												<tbody>\r\n"
							+ "													<tr>\r\n"
							+ "														<td>\r\n"
							+ "															<input\r\n"
							+ "																	type = \"text\"\r\n"
							+ "																	value = \"\"\r\n"
							+ "																	size = \"10\"\r\n"
							+ "																/>\r\n"
							+ "															<div id = \""+ idOfColumn +"\">\r\n"
							+ "																<br />\r\n"
							+ "																<button "
									+ "hx-get = \"/addInput/"+ idOfColumn +"\""
											+ "hx-target = \"#"+ idOfColumn +"\""
													+ "hx-swap = \"outerHTML\""
													+ ">\r\n"
							+ "																	+\r\n"
							+ "																	</button>\r\n"
							+ "																</div>\r\n"
							+ "															</td>\r\n"
							+ "														</tr>\r\n"
							+ "													</tbody>\r\n"
							+ "												</table>\r\n"
							+ "											</td>\r\n"
							+ "										<td>\r\n"
							+ "											<input\r\n"
							+ "													type = \"text\"\r\n"
							+ "													value = \"\"\r\n"
							+ "													size = \"5\"\r\n"
							+ "												/>\r\n"
							+ "											</td>\r\n"
							+ "										</tr>\r\n"
							+ "									</tbody>\r\n"
							+ "								</table>\r\n"
							+ "							</td>\r\n" );
				}
				else
				{
					entireBoardHtml.append( "						<td\r\n"
							+ "								class = \"td_clue\""
							+ "							>\r\n"
							+ "							&emsp;\r\n"
							+ "							</td>\r\n" );
				}
			}
			entireBoardHtml.append( "						</tr>\r\n" );
		}
		/*
		for ( Coordinate coordinate : board.getCells().allKeys() )
		{
			Cell cell = board.getCells().getVal( coordinate );
			
		}
		*/

		entireBoardHtml.append( "\t</tbody>\r\n<table>" );
		model.addAttribute( "entire_board", entireBoardHtml.toString() );

		return "base_page";
	}

}


















