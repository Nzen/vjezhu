/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.view;

/**

*/
public class ConsolasFont
{


	public static int pixelHeightOfCharacter(
			int fontSize
	) {
		if ( fontSize > 42 )
			throw new IllegalArgumentException( "consolas font not calculated for that big" );
		else if ( fontSize < 8 )
			throw new IllegalArgumentException( "consolas font not calculated for that small" );
		else if ( fontSize > 38 )
			return 27;
		else if ( fontSize > 33 )
			return 23;
		else if ( fontSize > 28 )
			return 19;
		else if ( fontSize > 23 )
			return 17;
		else if ( fontSize > 18 )
			return 13;
		else if ( fontSize > 13 )
			return 10;
		else // if ( fontSize >= 8 )
			return 6;
	}


	public static int pixelWidthOfCharacter(
			int fontSize
	) {
		if ( fontSize > 42 )
			throw new IllegalArgumentException( "consolas font not calculated for that big" );
		else if ( fontSize < 8 )
			throw new IllegalArgumentException( "consolas font not calculated for that small" );
		else if ( fontSize > 38 )
			return 18;
		else if ( fontSize > 33 )
			return 16;
		else if ( fontSize > 28 )
			return 14;
		else if ( fontSize > 23 )
			return 11;
		else if ( fontSize > 18 )
			return 9;
		else if ( fontSize > 13 )
			return 6;
		else // if ( fontSize >= 8 )
			return 4;
	}


	


}


















