/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.model.readonly;

import ws.nzen.game.pz.vjezhu.struct.ReadOnlyList;

/**

*/
public interface Group
{

	ReadOnlyList<? extends Coordinate> getCells();


	int getSumOfCellValues();

}


















