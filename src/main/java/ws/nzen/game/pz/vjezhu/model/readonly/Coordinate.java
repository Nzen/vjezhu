/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.model.readonly;

/**

*/
public interface Coordinate extends Comparable<Coordinate>
{

	public int getXxPosition();
	public int getYyPosition();


	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	default int compareTo(
			Coordinate another
	) {
		if ( getXxPosition() == another.getXxPosition() )
		{
			if ( getYyPosition() == another.getYyPosition() )
				return 0;
			else if ( getYyPosition() < another.getYyPosition() )
				return -1;
			else
				return 1;
		}
		else if ( getXxPosition() < another.getXxPosition() )
			return -1;
		else
			return 1;
	}

}


















