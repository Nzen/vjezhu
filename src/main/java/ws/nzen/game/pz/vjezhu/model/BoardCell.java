/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.model;


import ws.nzen.game.pz.vjezhu.model.readonly.Cell;
import ws.nzen.game.pz.vjezhu.struct.SemiMutableList;
import ws.nzen.game.pz.vjezhu.struct.SemiMutableList_Linked;


/**

*/
public class BoardCell implements Cell
{

	public static final int UNSET_VALUE = -1;

	private SemiMutableList<PossibilityCombo> columnGuesses = new SemiMutableList_Linked<>();
	private SemiMutableList<PossibilityCombo> rowGuesses = new SemiMutableList_Linked<>();
	private int userChosenValue = UNSET_VALUE;
	// IMPROVE puzzleSolvingValue


	public BoardCell(
	) {
	}


	public int getUserChosenValue()
	{
		return userChosenValue;
	}


	public void setUserChosenValue( int userChosenValue )
	{
		this.userChosenValue = userChosenValue;
	}


	public SemiMutableList<PossibilityCombo> getColumnGuesses()
	{
		return columnGuesses;
	}


	public SemiMutableList<PossibilityCombo> getRowGuesses()
	{
		return rowGuesses;
	}


	public void addPossibilityCombo(
			PossibilityCombo combo, boolean forRow
	) {
		if ( forRow )
			rowGuesses.add( combo );
		else
			columnGuesses.add( combo );
	}

}


















