/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.model.readonly;

import ws.nzen.game.pz.vjezhu.model.PossibilityCombo;
import ws.nzen.game.pz.vjezhu.struct.ReadOnlyList;

/**

*/
public interface Cell
{


	int getUserChosenValue();


	ReadOnlyList<PossibilityCombo> getColumnGuesses();


	ReadOnlyList<PossibilityCombo> getRowGuesses();


}


















