/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComponent;

import ws.nzen.game.pz.vjezhu.model.PossibilityCombo;
import ws.nzen.game.pz.vjezhu.model.PossibilityValue;
import ws.nzen.game.pz.vjezhu.model.Selection.CellElement;
import ws.nzen.game.pz.vjezhu.model.readonly.Board;
import ws.nzen.game.pz.vjezhu.model.readonly.Cell;
import ws.nzen.game.pz.vjezhu.model.readonly.Coordinate;
import ws.nzen.game.pz.vjezhu.model.readonly.Group;
import ws.nzen.game.pz.vjezhu.model.readonly.SelectionValues;

/**

*/
public class VjezhuCanvas extends JComponent
{


	private static final long serialVersionUID = 1L;
	private Board board;
	private SelectionValues cursor;


	public VjezhuCanvas(
			Board puzzle, SelectionValues selection
	) {
		board = puzzle;
		cursor = selection;
	}


	public void paintComponent(
			Graphics out
	) {
		super.paintComponent( out );

		Graphics2D flatOut = (Graphics2D)out;
		Stroke defaultLineStyle = flatOut.getStroke();
		Stroke cellBorderLineStyle = new BasicStroke( 2F );
		Stroke regionLineStyle = new BasicStroke(
				1F,
				BasicStroke.CAP_BUTT,
				BasicStroke.JOIN_BEVEL,
				0F,
				new float[]{ 5F, 5F },
				1F );

		Map<String, Integer> coordCombo_fontSize = new HashMap<>();
		final String rowSuffix = "r", columnSuffix = "c";

		int maxXx = 0, maxYy = 0;
		for ( Coordinate coordinate : board.getCells().allKeys() )
		{
			if ( coordinate.getXxPosition() > maxXx )
				maxXx = coordinate.getXxPosition();
			if ( coordinate.getYyPosition() > maxYy )
				maxYy = coordinate.getYyPosition();
		}

		int entireCellPaddingWidth = 6, entireCellPaddingHeight = 6;
		int cellWidth = (int)Math.floor( ( getWidth() - entireCellPaddingWidth ) / (double)( maxXx +1 ) );
		int cellHeight = (int)Math.floor( ( getHeight() - entireCellPaddingHeight ) / (double)( maxYy +1 ) );

		// draw cell contents
		for ( Coordinate coordinate : board.getCells().allKeys() )
		{
			flatOut.setStroke( cellBorderLineStyle );
			flatOut.drawRect(
					cellWidth * coordinate.getXxPosition(),
					cellHeight * coordinate.getYyPosition(),
					cellWidth,
					cellHeight );
			int cellRegionWidth = (int)Math.floor( cellWidth * 0.4 );
			// divide row / column possibilities
			flatOut.setStroke( regionLineStyle );
			int regionLinePadding = 10;
			flatOut.drawLine(
					cellWidth * coordinate.getXxPosition() + cellRegionWidth,
					cellHeight * coordinate.getYyPosition() + regionLinePadding,
					cellWidth * coordinate.getXxPosition() + cellRegionWidth,
					cellHeight * ( coordinate.getYyPosition() +1 ) - regionLinePadding );
			// divide column possibilities from committed value
			flatOut.drawLine(
					cellWidth * coordinate.getXxPosition() + ( cellRegionWidth * 2 ),
					cellHeight * coordinate.getYyPosition() + regionLinePadding,
					cellWidth * coordinate.getXxPosition() + ( cellRegionWidth * 2 ),
					cellHeight * ( coordinate.getYyPosition() +1 ) - regionLinePadding );

			flatOut.setStroke( defaultLineStyle );
			Cell cell = board.getCells().getVal( coordinate );
			int regionSection = 0;

			// write row p combos in leftmost
			if ( ! cell.getRowGuesses().empty() )
			{
				int charCount = cell.getRowGuesses().getVal( 0 ).getValues().size(); // fix check if it is empty
				// choose font size and columns for width
				int fontSize = 30, columns = 1, kerningPixelsWidth = 0, pixelCharMargin = 5;
				int charWidth = ConsolasFont.pixelWidthOfCharacter( fontSize );
				int charHeight = ConsolasFont.pixelHeightOfCharacter( fontSize );
				kerningPixelsWidth = (int)Math.floor( charWidth / 2D );
				pixelCharMargin = (int)Math.ceil( charHeight / 10D );
				int comboWidth = charCount * ( charWidth + kerningPixelsWidth );
				int comboHeight = charHeight + pixelCharMargin;
				while ( comboWidth * columns > cellRegionWidth )
				{
					fontSize -= 5;
					charWidth = ConsolasFont.pixelWidthOfCharacter( fontSize );
					charHeight = ConsolasFont.pixelHeightOfCharacter( fontSize );
					kerningPixelsWidth = (int)Math.floor( charWidth / 2D );
					pixelCharMargin = (int)Math.ceil( charHeight / 10D );
					comboWidth = charCount * ( charWidth + kerningPixelsWidth );
					comboHeight = charHeight + pixelCharMargin;
					if ( fontSize < 5 )
						throw new RuntimeException( "too many possibilities to fit horizontally" );
				}
				// ensure this fits vertically
				while ( ( comboHeight * cell.getRowGuesses().len() ) / columns > ( cellHeight - pixelCharMargin ) )
				{
					if ( comboWidth * columns +1 < cellRegionWidth )
					{
						columns++;
						continue;
					}
					fontSize -= 5;
					comboWidth = charCount * ( charWidth + kerningPixelsWidth );
					comboHeight = charHeight + pixelCharMargin;
					if ( fontSize < 1 )
						throw new RuntimeException( "too many possibilities to fit vertically" );
				}
				out.setFont( new Font( "Consolas", 0, fontSize ) ); // fix cache these
				coordCombo_fontSize.put( coordinate.toString() + rowSuffix, fontSize );
				int regionColumnIndex = 0;
				int regionRow = 0, rowColumnIndex = 0;
				for ( PossibilityCombo combo : cell.getRowGuesses() )
				{
					for ( int cvInd = 0; cvInd < combo.getValues().size(); cvInd++ )
					{
						PossibilityValue pcValue = combo.getValues().get( cvInd );
						int xxPixelOfChar = ( coordinate.getXxPosition() * cellWidth )
								+ kerningPixelsWidth
								+ ( regionColumnIndex * comboWidth * rowColumnIndex )
								+ ( cvInd * charWidth )
								+ ( cellRegionWidth * regionSection );
						int yyPixelOfChar = ( coordinate.getYyPosition() * cellHeight )
								+ charHeight // because this draws from the bottom, not the top
								+ pixelCharMargin
								+ ( comboHeight * regionRow );
						out.drawString(
								Integer.toString( pcValue.getChosenValue() ),
								xxPixelOfChar,
								yyPixelOfChar );
						/* enable later
						if ( pcValue.slash != slash.none )
							drawSlash( pcValue.slash, xxPixelOfChar, yyPixelOfChar )
						*/
					}
					if ( columns == 1 )
						regionRow++;
					else
					{
						if ( rowColumnIndex < columns )
							rowColumnIndex++;
						else
						{
							rowColumnIndex = 0;
							regionRow++;
						}
					}
				}
			}

			regionSection++;
			// write row p combos in leftmost
			if ( ! cell.getColumnGuesses().empty() )
			{
				int charCount = cell.getColumnGuesses().getVal( 0 ).getValues().size(); // fix check if it is empty
				// choose font size and columns for width
				int fontSize = 30, columns = 1, kerningPixelsWidth = 0, pixelCharMargin = 5;
				int charWidth = ConsolasFont.pixelWidthOfCharacter( fontSize );
				int charHeight = ConsolasFont.pixelHeightOfCharacter( fontSize );
				kerningPixelsWidth = (int)Math.floor( charWidth / 2D );
				pixelCharMargin = (int)Math.ceil( charHeight / 10D );
				int comboWidth = charCount * ( charWidth + kerningPixelsWidth );
				int comboHeight = charHeight + pixelCharMargin;
				while ( comboWidth * columns > cellRegionWidth )
				{
					fontSize -= 5;
					charWidth = ConsolasFont.pixelWidthOfCharacter( fontSize );
					charHeight = ConsolasFont.pixelHeightOfCharacter( fontSize );
					kerningPixelsWidth = (int)Math.floor( charWidth / 2D );
					pixelCharMargin = (int)Math.ceil( charHeight / 10D );
					comboWidth = charCount * ( charWidth + kerningPixelsWidth );
					comboHeight = charHeight + pixelCharMargin;
					if ( fontSize < 5 )
						throw new RuntimeException( "too many possibilities to fit horizontally" );
				}
				// ensure this fits vertically
				while ( ( comboHeight * cell.getColumnGuesses().len() ) / columns > ( cellHeight - pixelCharMargin ) )
				{
					if ( comboWidth * columns +1 < cellRegionWidth )
					{
						columns++;
						continue;
					}
					fontSize -= 5;
					comboWidth = charCount * ( charWidth + kerningPixelsWidth );
					comboHeight = charHeight + pixelCharMargin;
					if ( fontSize < 1 )
						throw new RuntimeException( "too many possibilities to fit vertically" );
				}
				out.setFont( new Font( "Consolas", 0, fontSize ) ); // fix cache these
				coordCombo_fontSize.put( coordinate.toString() + columnSuffix, fontSize );
				int regionColumnIndex = 0;
				int regionRow = 0, rowColumnIndex = 0;
				for ( PossibilityCombo combo : cell.getColumnGuesses() )
				{
					for ( int cvInd = 0; cvInd < combo.getValues().size(); cvInd++ )
					{
						PossibilityValue pcValue = combo.getValues().get( cvInd );
						int xxPixelOfChar = ( coordinate.getXxPosition() * cellWidth )
								+ kerningPixelsWidth
								+ ( regionColumnIndex * comboWidth * rowColumnIndex )
								+ ( cvInd * charWidth )
								+ ( cellRegionWidth * regionSection );
						int yyPixelOfChar = ( coordinate.getYyPosition() * cellHeight )
								+ charHeight // because this draws from the bottom, not the top
								+ pixelCharMargin
								+ ( comboHeight * regionRow );
						out.drawString(
								Integer.toString( pcValue.getChosenValue() ),
								xxPixelOfChar,
								yyPixelOfChar );
						/* enable later
						if ( pcValue.slash != slash.none )
							drawSlash( pcValue.slash, xxPixelOfChar, yyPixelOfChar )
						*/
					}
					if ( columns == 1 )
						regionRow++;
					else
					{
						if ( rowColumnIndex < columns )
							rowColumnIndex++;
						else
						{
							rowColumnIndex = 0;
							regionRow++;
						}
					}
				}
			}

			// write committed value
			if ( cell.getUserChosenValue() > 0 )
			{
				int fontSize = 40, pixelCharMargin;
				int regionWidthLeft = cellWidth - ( cellRegionWidth * 2 );
				int charWidth = ConsolasFont.pixelWidthOfCharacter( fontSize );
				pixelCharMargin = (int)Math.ceil( charWidth / 10D );
				while ( charWidth > regionWidthLeft )
				{
					fontSize -= 5;
					charWidth = ConsolasFont.pixelWidthOfCharacter( fontSize );
					pixelCharMargin = (int)Math.ceil( charWidth / 10D );
					if ( fontSize < 5 )
						throw new RuntimeException( "too thin to draw committed value" );
				}
				out.setFont( new Font( "Consolas", 0, fontSize ) ); // fix cache these
				int xxPixelOfChar = ( coordinate.getXxPosition() * cellWidth )
						+ ( cellRegionWidth * 2 )
						+ (int)Math.floor( charWidth / 2D );
				int yyPixelOfChar = ( coordinate.getYyPosition() * cellHeight )
						+ ( cellHeight /2 )
						+ pixelCharMargin;
				out.drawString(
						Integer.toString( cell.getUserChosenValue() ),
						xxPixelOfChar,
						yyPixelOfChar );
			}

			int regionWidthLeft = cellWidth - ( cellRegionWidth * 2 );
			int fontSize = 40;
			int charWidth = ConsolasFont.pixelWidthOfCharacter( fontSize );
			while ( charWidth > regionWidthLeft )
			{
				fontSize -= 5;
				charWidth = ConsolasFont.pixelWidthOfCharacter( fontSize );
				if ( fontSize < 5 )
					throw new RuntimeException( "too thin to draw clue" );
			}

			// draw linear group constraints: row
			for ( Group row : board.getRows() )
			{
				coordinate = row.getCells().getVal( 0 );
				// diagonal from top left to bottom right, one cell to the left of the coordinate
				flatOut.drawLine(
						cellWidth * ( coordinate.getXxPosition() -1 ),
						cellHeight * coordinate.getYyPosition(),
						cellWidth * coordinate.getXxPosition(),
						cellHeight * ( coordinate.getYyPosition() +1 ) );
				int xxPixelOfChar = ( ( coordinate.getXxPosition() -1 ) * cellWidth )
						+ ( regionWidthLeft * 3 )
						+ (int)Math.floor( charWidth / 2D );
				int yyPixelOfChar = ( coordinate.getYyPosition() * cellHeight )
						+ ( cellHeight /2 );
				out.setFont( new Font( "Consolas", 0, fontSize ) ); // fix cache these
				out.drawString(
						Integer.toString( row.getSumOfCellValues() ),
						xxPixelOfChar,
						yyPixelOfChar );
			}

			// draw linear group constraints: column
			for ( Group column : board.getColumns() )
			{
				coordinate = column.getCells().getVal( 0 );
				// diagonal from top left to bottom right, one cell to the left of the coordinate
				flatOut.drawLine(
						cellWidth * coordinate.getXxPosition(),
						cellHeight * ( coordinate.getYyPosition() -1 ),
						cellWidth * ( coordinate.getXxPosition() +1 ),
						cellHeight * coordinate.getYyPosition() );
				int xxPixelOfChar = ( coordinate.getXxPosition() * cellWidth )
						+ ( regionWidthLeft )
						+ (int)Math.floor( charWidth / 2D );
				int yyPixelOfChar = ( ( coordinate.getYyPosition() -1 ) * cellHeight )
						+ ( (int)Math.floor( cellHeight * 0.75 ) );
				out.setFont( new Font( "Consolas", 0, fontSize ) ); // fix cache these
				out.drawString(
						Integer.toString( column.getSumOfCellValues() ),
						xxPixelOfChar,
						yyPixelOfChar );
			}

		}

		// draw the selection
		out.setColor( new Color( 255, 0, 0 ) ); // fix cache these
		if ( cursor.getPossibleValueIndex() >= 0 )
		{
			if ( cursor.getRegion() == CellElement.ROW_COMBINATIONS )
			{
				
			}
			else // if ( cursor.getRegion() == CellElement.COLUMN_COMBINATIONS )
			{
			}
		}
		else if ( cursor.getCombinationIndex() >= 0 )
		{
			int cellRegionWidth = (int)Math.floor( cellWidth * 0.4 );
			Cell cell = board.getCells().getVal( cursor.getCell() );
			if ( cursor.getRegion() == CellElement.ROW_COMBINATIONS )
			{
				int fontSize = coordCombo_fontSize.get( cursor.getCell() + rowSuffix );
				int kerningPixelsWidth = 0, pixelCharMargin = 5;
				int charWidth = ConsolasFont.pixelWidthOfCharacter( fontSize );
				int charHeight = ConsolasFont.pixelHeightOfCharacter( fontSize );
				kerningPixelsWidth = (int)Math.floor( charWidth / 2D );
				pixelCharMargin = (int)Math.ceil( charHeight / 10D );
				int comboWidth = cell.getRowGuesses().getVal( 0 ).getValues().size()
						* ( charWidth + kerningPixelsWidth );
				int comboHeight = charHeight + pixelCharMargin;
				// fix handle columns
				flatOut.drawRect(
						cellWidth * cursor.getCell().getXxPosition() + kerningPixelsWidth,
						cellHeight * cursor.getCell().getYyPosition() + pixelCharMargin + ( comboHeight * cursor.getCombinationIndex() ),
						comboWidth,
						comboHeight );
			}
			else // if ( cursor.getRegion() == CellElement.COLUMN_COMBINATIONS )
			{
				int fontSize = coordCombo_fontSize.get( cursor.getCell() + columnSuffix );
				int kerningPixelsWidth = 0, pixelCharMargin = 5;
				int charWidth = ConsolasFont.pixelWidthOfCharacter( fontSize );
				int charHeight = ConsolasFont.pixelHeightOfCharacter( fontSize );
				kerningPixelsWidth = (int)Math.floor( charWidth / 2D );
				pixelCharMargin = (int)Math.ceil( charHeight / 10D );
				int comboWidth = cell.getColumnGuesses().getVal( 0 ).getValues().size()
						* ( charWidth + kerningPixelsWidth );
				int comboHeight = charHeight + pixelCharMargin;
				// fix handle columns
				flatOut.drawRect(
						cellWidth * cursor.getCell().getXxPosition() + kerningPixelsWidth + cellRegionWidth,
						cellHeight * cursor.getCell().getYyPosition() + pixelCharMargin + ( comboHeight * cursor.getCombinationIndex() ),
						comboWidth,
						comboHeight );
			}
		}
		else if ( cursor.getRegion() != CellElement.NON_REGION )
		{
			int cellRegionWidth = (int)Math.floor( cellWidth * 0.4 );
			if ( cursor.getRegion() == CellElement.ROW_COMBINATIONS )
			{
				flatOut.drawRect(
						cellWidth * cursor.getCell().getXxPosition() + entireCellPaddingWidth,
						cellHeight * cursor.getCell().getYyPosition() + entireCellPaddingHeight,
						cellRegionWidth - ( entireCellPaddingHeight *2 ),
						cellHeight - ( entireCellPaddingHeight *2 ) );
			}
			else if ( cursor.getRegion() == CellElement.COLUMN_COMBINATIONS )
			{
				flatOut.drawRect(
						cellWidth * cursor.getCell().getXxPosition() + entireCellPaddingWidth + cellRegionWidth,
						cellHeight * cursor.getCell().getYyPosition() + entireCellPaddingHeight,
						cellRegionWidth - ( entireCellPaddingHeight *2 ),
						cellHeight - ( entireCellPaddingHeight *2 ) );
			}
			else // committed value
			{
				int regionWidthLeft = cellWidth - ( cellRegionWidth *2 );
				flatOut.drawRect(
						cellWidth * cursor.getCell().getXxPosition() + entireCellPaddingWidth + ( cellRegionWidth *2 ),
						cellHeight * cursor.getCell().getYyPosition() + entireCellPaddingHeight,
						regionWidthLeft - ( entireCellPaddingHeight *2 ),
						cellHeight - ( entireCellPaddingHeight *2 ) );
			}
		}
		else // just a cell selection
		{
			flatOut.drawRect(
					cellWidth * cursor.getCell().getXxPosition() + entireCellPaddingWidth,
					cellHeight * cursor.getCell().getYyPosition() + entireCellPaddingHeight,
					cellWidth - ( entireCellPaddingWidth *2 ),
					cellHeight - ( entireCellPaddingHeight *2 ) );
		}
		/*
		
		struct sizesForCell(
				collection<possibilityCombos> combos, cellWidth, cellHeight )
			characterWidth = cell.possibilities.row.get( 0 ).length
			// choose font size and columns
			int fontSize = 18, columns = 1
			int comboWidth = widthOfPossible( charWidth, fontSize )
			int comboHeight = heightOfPossible( charWidth, fontSize )
			while ( comboWidth * columns > cellRegionWidth )
				fontSize--
				comboWidth = widthOfPossible( charWidth, fontSize )
				comboHeight = heightOfPossible( charWidth, fontSize )
				if ( fontSize < 1 )
					throw new runtimeexception( too many possibilities to fit horizontally )
			// assuming this doesn't become size 2
			while ( ( comboHeight * cell.possibilities.row.len ) / columns > ( cellHeight - padding ) )
				if ( widthOfPossible( charWidth, fontSize ) * columns +1 < cellRegionWidth )
					columns++
					continue
				fontSize--
				comboWidth = widthOfPossible( charWidth, fontSize )
				comboHeight = heightOfPossible( charWidth, fontSize )
				if ( fontSize < 1 )
					throw new runtimeexception( too many possibilities to fit vertically )
		*/
	}


}


















