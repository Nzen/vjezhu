/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.model;

import java.util.Objects;

import ws.nzen.game.pz.vjezhu.model.readonly.Coordinate;

/**

*/
public class BoardCoordinate
		implements Coordinate
{
	private int xxPosition;
	private int yyPosition;


	public BoardCoordinate(
			int xxSuggested, int yySuggested
	) {
		if ( xxSuggested < 0 )
			throw new IllegalArgumentException( "x coordinate must not be negative" );
		if ( yySuggested < 0 )
			throw new IllegalArgumentException( "y coordinate must not be negative" );
		xxPosition = xxSuggested;
		yyPosition = yySuggested;
	}


	@Override
	public int hashCode()
	{
		return Objects.hash( xxPosition, yyPosition );
	}


	@Override
	public boolean equals( Object obj )
	{
		if ( this == obj )
			return true;
		if ( obj == null )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		BoardCoordinate other = (BoardCoordinate) obj;
		return xxPosition == other.xxPosition && yyPosition == other.yyPosition;
	}


	@Override
	public int getXxPosition()
	{
		return xxPosition;
	}


	@Override
	public int getYyPosition()
	{
		return yyPosition;
	}


	public void setXxPosition( int xxSuggested )
	{
		if ( xxSuggested < 0 )
			throw new IllegalArgumentException( "x coordinate must not be negative" );
		xxPosition = xxSuggested;
	}


	public void setYyPosition( int yySuggested )
	{
		if ( yySuggested < 0 )
			throw new IllegalArgumentException( "y coordinate must not be negative" );
		yyPosition = yySuggested;
	}


	@Override
	public String toString()
	{
		return Integer.toString( xxPosition ) +":"+ Integer.toString( yyPosition );
	}


}


















