/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.struct;

import java.util.Map;

/**
A superset of Map and ReadOnlyMap.
*/
public interface SemiMutableMap<Key, Value> extends Map<Key, Value>, ReadOnlyMap<Key, Value>
{




}


















