/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.struct;

import java.util.Set;

/**
A superset of Set and ReadOnlySet.
*/
public interface SemiMutableSet<Type> extends Set<Type>, ReadOnlySet<Type>
{




}


















