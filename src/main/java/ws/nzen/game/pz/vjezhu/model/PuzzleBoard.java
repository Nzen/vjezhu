/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.model;

import ws.nzen.game.pz.vjezhu.model.readonly.Board;
import ws.nzen.game.pz.vjezhu.model.readonly.Coordinate;
import ws.nzen.game.pz.vjezhu.struct.SemiMutableCollection;
import ws.nzen.game.pz.vjezhu.struct.SemiMutableList_Linked;
import ws.nzen.game.pz.vjezhu.struct.SemiMutableMap;
import ws.nzen.game.pz.vjezhu.struct.SemiMutableMap_Hash;

/**

*/
public class PuzzleBoard implements Board
{

	private SemiMutableMap<Coordinate, BoardCell> cells = new SemiMutableMap_Hash<>();
	private SemiMutableCollection<LinearGroup> rows = new SemiMutableList_Linked<>();
	private SemiMutableCollection<LinearGroup> columns = new SemiMutableList_Linked<>();


	public PuzzleBoard(
	) {
		
	}


	public SemiMutableMap<Coordinate, BoardCell> getCells(
	) {
		return cells;
	}


	public SemiMutableCollection<LinearGroup> getRows()
	{
		return rows;
	}


	public SemiMutableCollection<LinearGroup> getColumns()
	{
		return columns;
	}


}


















