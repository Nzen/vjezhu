/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.view;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import ws.nzen.game.pz.vjezhu.model.readonly.Board;
import ws.nzen.game.pz.vjezhu.model.readonly.SelectionValues;

/**

*/
public class VjezhuFrame extends JFrame
{


	public VjezhuFrame(
			Board board, SelectionValues cursor
	) {
		initComponents( board, cursor );
	}


    private void initComponents(
    		Board board, SelectionValues cursor
    ) {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    	setTitle( "Vexing Juniper Ezhu" );
    	setSize( new Dimension( 1500, 1500 ) );
    	add( new VjezhuCanvas( board, cursor ) );
    }

}


















