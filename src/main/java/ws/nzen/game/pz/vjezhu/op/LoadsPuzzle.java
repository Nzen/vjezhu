/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.op;

import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import ws.nzen.game.pz.vjezhu.model.BoardCell;
import ws.nzen.game.pz.vjezhu.model.BoardCoordinate;
import ws.nzen.game.pz.vjezhu.model.LinearGroup;
import ws.nzen.game.pz.vjezhu.model.PossibilityCombo;
import ws.nzen.game.pz.vjezhu.model.PossibilityValue;
import ws.nzen.game.pz.vjezhu.model.PuzzleBoard;
import ws.nzen.game.pz.vjezhu.model.readonly.Coordinate;
import ws.nzen.game.pz.vjezhu.struct.SemiMutableCollection;
import ws.nzen.game.pz.vjezhu.struct.SemiMutableList;
import ws.nzen.game.pz.vjezhu.struct.SemiMutableList_Linked;
import ws.nzen.game.pz.vjezhu.struct.SemiMutableMap;

/**

*/
public class LoadsPuzzle
{


	public PuzzleBoard fillWithHandCopiedPuzzle(
			PuzzleBoard sink
	) {
		SemiMutableMap<Coordinate, BoardCell> cells = sink.getCells();

		Random oracle = new Random();
		int dieRoll, dieThreshold = 5;

		BoardCell cell1_1 = new BoardCell();
		addCombosToCell( cell1_1, 2, true, oracle );
		addCombosToCell( cell1_1, 2, false, oracle );
		BoardCoordinate bc11 = new BoardCoordinate( 1, 1 );
		cells.put( bc11, cell1_1 );
		BoardCell cell1_2 = new BoardCell();
		addCombosToCell( cell1_2, 2, true, oracle );
		addCombosToCell( cell1_2, 5, false, oracle );
		dieRoll = oracle.nextInt( 20 );
		if ( dieRoll > dieThreshold )
			cell1_2.setUserChosenValue( oracle.nextInt( 9 ) );
		BoardCoordinate bc12 = new BoardCoordinate( 1, 2 );
		cells.put( bc12, cell1_2 );

		BoardCell cell1_4 = new BoardCell();
		addCombosToCell( cell1_4, 5, true, oracle );
		addCombosToCell( cell1_4, 2, false, oracle );
		dieRoll = oracle.nextInt( 11 );
		if ( dieRoll > dieThreshold )
			cell1_4.setUserChosenValue( oracle.nextInt( 9 ) );
		BoardCoordinate bc14 = new BoardCoordinate( 1, 4 );
		cells.put( bc14, cell1_4 );
		BoardCell cell1_5 = new BoardCell();
		addCombosToCell( cell1_5, 2, true, oracle );
		addCombosToCell( cell1_5, 2, false, oracle );
		BoardCoordinate bc15 = new BoardCoordinate( 1, 5 );
		cells.put( bc15, cell1_5 );

		BoardCell cell2_1 = new BoardCell();
		addCombosToCell( cell2_1, 2, true, oracle );
		addCombosToCell( cell2_1, 5, false, oracle );
		BoardCoordinate bc21 = new BoardCoordinate( 2, 1 );
		cells.put( bc21, cell2_1 );
		BoardCell cell2_2 = new BoardCell();
		addCombosToCell( cell2_2, 5, true, oracle );
		addCombosToCell( cell2_2, 5, false, oracle );
		BoardCoordinate bc22 = new BoardCoordinate( 2, 2 );
		cells.put( bc22, cell2_2 );
		BoardCell cell2_3 = new BoardCell();
		addCombosToCell( cell2_3, 3, true, oracle );
		addCombosToCell( cell2_3, 5, false, oracle );
		dieRoll = oracle.nextInt( 11 );
		if ( dieRoll > dieThreshold )
			cell2_3.setUserChosenValue( oracle.nextInt( 9 ) );
		BoardCoordinate bc23 = new BoardCoordinate( 2, 3 );
		cells.put( bc23, cell2_3 );
		BoardCell cell2_4 = new BoardCell();
		addCombosToCell( cell2_4, 5, true, oracle );
		addCombosToCell( cell2_4, 5, false, oracle );
		BoardCoordinate bc24 = new BoardCoordinate( 2, 4 );
		cells.put( bc24, cell2_4 );
		BoardCell cell2_5 = new BoardCell();
		addCombosToCell( cell2_5, 2, true, oracle );
		addCombosToCell( cell2_5, 5, false, oracle );
		BoardCoordinate bc25 = new BoardCoordinate( 2, 5 );
		cells.put( bc25, cell2_5 );		

		BoardCell cell3_2 = new BoardCell();
		addCombosToCell( cell3_2, 5, true, oracle );
		addCombosToCell( cell3_2, 3, false, oracle );
		BoardCoordinate bc32 = new BoardCoordinate( 3, 2 );
		cells.put( bc32, cell3_2 );
		BoardCell cell3_3 = new BoardCell();
		addCombosToCell( cell3_3, 3, true, oracle );
		addCombosToCell( cell3_3, 3, false, oracle );
		BoardCoordinate bc33 = new BoardCoordinate( 3, 3 );
		cells.put( bc33, cell3_3 );
		BoardCell cell3_4 = new BoardCell();
		addCombosToCell( cell3_4, 5, true, oracle );
		addCombosToCell( cell3_4, 3, false, oracle );
		dieRoll = oracle.nextInt( 11 );
		if ( dieRoll > dieThreshold )
			cell3_4.setUserChosenValue( oracle.nextInt( 9 ) );
		BoardCoordinate bc34 = new BoardCoordinate( 3, 4 );
		cells.put( bc34, cell3_4 );

		BoardCell cell4_1 = new BoardCell();
		addCombosToCell( cell4_1, 2, true, oracle );
		addCombosToCell( cell4_1, 5, false, oracle );
		BoardCoordinate bc41 = new BoardCoordinate( 4, 1 );
		cells.put( bc41, cell4_1 );
		BoardCell cell4_2 = new BoardCell();
		addCombosToCell( cell4_2, 5, true, oracle );
		addCombosToCell( cell4_2, 5, false, oracle );
		BoardCoordinate bc42 = new BoardCoordinate( 4, 2 );
		cells.put( bc42, cell4_2 );
		BoardCell cell4_3 = new BoardCell();
		addCombosToCell( cell4_3, 3, true, oracle );
		addCombosToCell( cell4_3, 5, false, oracle );
		BoardCoordinate bc43 = new BoardCoordinate( 4, 3 );
		cells.put( bc43, cell4_3 );
		BoardCell cell4_4 = new BoardCell();
		addCombosToCell( cell4_4, 2, true, oracle );
		addCombosToCell( cell4_4, 5, false, oracle );
		BoardCoordinate bc44 = new BoardCoordinate( 4, 4 );
		cells.put( bc44, cell4_4 );
		BoardCell cell4_5 = new BoardCell();
		addCombosToCell( cell4_5, 2, true, oracle );
		addCombosToCell( cell4_5, 2, false, oracle );
		dieRoll = oracle.nextInt( 11 );
		if ( dieRoll > dieThreshold )
			cell4_5.setUserChosenValue( oracle.nextInt( 9 ) );
		BoardCoordinate bc45 = new BoardCoordinate( 4, 5 );
		cells.put( bc45, cell4_5 );

		BoardCell cell5_1 = new BoardCell();
		addCombosToCell( cell5_1, 2, true, oracle );
		addCombosToCell( cell5_1, 2, false, oracle );
		BoardCoordinate bc51 = new BoardCoordinate( 5, 1 );
		cells.put( bc51, cell5_1 );
		BoardCell cell5_2 = new BoardCell();
		addCombosToCell( cell5_2, 5, true, oracle );
		addCombosToCell( cell5_2, 2, false, oracle );
		dieRoll = oracle.nextInt( 11 );
		if ( dieRoll > dieThreshold )
			cell5_2.setUserChosenValue( oracle.nextInt( 9 ) );
		BoardCoordinate bc52 = new BoardCoordinate( 5, 2 );
		cells.put( bc52, cell5_2 );

		BoardCell cell5_4 = new BoardCell();
		addCombosToCell( cell5_4, 5, true, oracle );
		addCombosToCell( cell5_4, 2, false, oracle );
		BoardCoordinate bc54 = new BoardCoordinate( 5, 4 );
		cells.put( bc54, cell5_4 );
		BoardCell cell5_5 = new BoardCell();
		addCombosToCell( cell5_5, 2, true, oracle );
		addCombosToCell( cell5_5, 2, false, oracle );
		dieRoll = oracle.nextInt( 11 );
		if ( dieRoll > dieThreshold )
			cell5_5.setUserChosenValue( oracle.nextInt( 9 ) );
		BoardCoordinate bc55 = new BoardCoordinate( 5, 5 );
		cells.put( bc55, cell5_5 );

		// cells.put( new BoardCoordinate( 9, 17 ),  );

		SemiMutableCollection<LinearGroup> rows = sink.getRows();
		SemiMutableList<Coordinate> someGroup = new SemiMutableList_Linked<>();
		someGroup.add( bc11 );
		someGroup.add( bc21 );
		rows.add( new LinearGroup( 5, someGroup ) );

		someGroup = new SemiMutableList_Linked<>();
		someGroup.add( bc41 );
		someGroup.add( bc51 );
		rows.add( new LinearGroup( 6, someGroup ) );

		someGroup = new SemiMutableList_Linked<>();
		someGroup.add( bc12 );
		someGroup.add( bc22 );
		someGroup.add( bc32 );
		someGroup.add( bc42 );
		someGroup.add( bc42 );
		rows.add( new LinearGroup( 15, someGroup ) );

		someGroup = new SemiMutableList_Linked<>();
		someGroup.add( bc23 );
		someGroup.add( bc33 );
		someGroup.add( bc43 );
		rows.add( new LinearGroup( 7, someGroup ) );

		someGroup = new SemiMutableList_Linked<>();
		someGroup.add( bc14 );
		someGroup.add( bc24 );
		someGroup.add( bc34 );
		someGroup.add( bc44 );
		someGroup.add( bc54 );
		rows.add( new LinearGroup( 15, someGroup ) );

		someGroup = new SemiMutableList_Linked<>();
		someGroup.add( bc15 );
		someGroup.add( bc25 );
		rows.add( new LinearGroup( 3, someGroup ) );

		someGroup = new SemiMutableList_Linked<>();
		someGroup.add( bc45 );
		someGroup.add( bc55 );
		rows.add( new LinearGroup( 4, someGroup ) );

		SemiMutableCollection<LinearGroup> columns = sink.getColumns();

		someGroup = new SemiMutableList_Linked<>();
		someGroup.add( bc11 );
		someGroup.add( bc12 );
		columns.add( new LinearGroup( 5, someGroup ) );

		someGroup = new SemiMutableList_Linked<>();
		someGroup.add( bc14);
		someGroup.add( bc15 );
		columns.add( new LinearGroup( 3, someGroup ) );

		someGroup = new SemiMutableList_Linked<>();
		someGroup.add( bc21 );
		someGroup.add( bc22 );
		someGroup.add( bc23 );
		someGroup.add( bc24 );
		someGroup.add( bc25 );
		columns.add( new LinearGroup( 15, someGroup ) );

		someGroup = new SemiMutableList_Linked<>();
		someGroup.add( bc32 );
		someGroup.add( bc33 );
		someGroup.add( bc34 );
		columns.add( new LinearGroup( 8, someGroup ) );

		someGroup = new SemiMutableList_Linked<>();
		someGroup.add( bc41 );
		someGroup.add( bc42 );
		someGroup.add( bc43 );
		someGroup.add( bc44 );
		someGroup.add( bc45 );
		columns.add( new LinearGroup( 15, someGroup ) );

		someGroup = new SemiMutableList_Linked<>();
		someGroup.add( bc51 );
		someGroup.add( bc52 );
		columns.add( new LinearGroup( 3, someGroup ) );

		someGroup = new SemiMutableList_Linked<>();
		someGroup.add( bc54 );
		someGroup.add( bc55 );
		columns.add( new LinearGroup( 6, someGroup ) );

		return sink;
	}


	private void addCombosToCell(
			BoardCell cell, int valueCount, boolean rowCombos, Random oracle
	) {
		int combos = oracle.nextInt( valueCount ) +1;
		Set<Integer> chosen = new TreeSet<>();
		for ( ; combos > 0; combos-- )
		{
			SemiMutableList<PossibilityValue> values = new SemiMutableList_Linked<>();
			chosen.clear();
			for ( int val = 0; val < valueCount; val++ )
			{
				int temp = oracle.nextInt( valueCount ) +1;
				while ( chosen.contains( temp ) )
					temp = oracle.nextInt( valueCount );
				values.add( new PossibilityValue( temp ) );
				chosen.add( temp );
			}
			cell.addPossibilityCombo( new PossibilityCombo( values ), rowCombos );
		}
	}


}


















