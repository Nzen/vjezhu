/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.struct;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Stream;

/**

*/
public class SemiMutableMap_Hash<Key, Value> extends HashMap<Key, Value> implements SemiMutableMap<Key, Value>
{


	private static final long serialVersionUID = 1L;


	public SemiMutableMap_Hash(
	) {
		super();
	}


	public SemiMutableMap_Hash(
		int initialSize
	) {
		super( initialSize );
	}


	public SemiMutableMap_Hash(
		Map<Key, Value> adopt
	) {
		super( adopt );
	}


	public SemiMutableMap_Hash(
		ReadOnlyMap<Key, Value> stuff
	) {
		super();
		Iterator<Key> loop = stuff.allKeys().iterator();
		while( loop.hasNext() )
		{
			Key key = loop.next();
			put( key, stuff.getVal( key ) );
		}
	}


	@Override
	public ReadOnlySet<Key> allKeys(
	) {
		return new SemiMutableSet_Hash<>( keySet() );
	}


	@Override
	public ReadOnlyCollection<Value> allValues(
	) {
		return new SemiMutableList_Linked<>( values() );
	}


	@Override
	public boolean empty(
	) {
		return isEmpty();
	}


	@Override
	public Value getOrFallback(
		Key unknown, Value fallback
	) {
		return getOrDefault( unknown, fallback );
	}


	@Override
	public Value getVal(
		Key unknown
	) {
		return get( unknown );
	}


	@Override
	public boolean has(
		Key unknown
	) {
		return containsKey( unknown );
	}


	@Override
	public boolean hasVal(
		Value unknown
	) {
		return containsValue( unknown );
	}


	@Override
	public Iterator<Key> iterator(
	) {
		return keySet().iterator();
	}


	@Override
	public int len(
	) {
		return size();
	}


	@Override
	/** Key stream, specifically. */
	public Stream<Key> streamOf(
	) {
		return keySet().stream();
	}


}


















