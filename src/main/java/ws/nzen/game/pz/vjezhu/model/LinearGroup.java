/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.model;

import ws.nzen.game.pz.vjezhu.model.readonly.Coordinate;
import ws.nzen.game.pz.vjezhu.model.readonly.Group;
import ws.nzen.game.pz.vjezhu.struct.SemiMutableList;
import ws.nzen.game.pz.vjezhu.struct.SemiMutableList_Linked;

/**

*/
public class LinearGroup implements Group
{

	private SemiMutableList<Coordinate> cells = new SemiMutableList_Linked<>();
	private int sumOfCellValues;


	public LinearGroup(
			int targetValue, SemiMutableList<Coordinate> cellsToUse
	) {
		sumOfCellValues = targetValue;
		cells.addAll( cellsToUse );
	}


	public SemiMutableList<? extends Coordinate> getCells()
	{
		return cells;
	}


	public int getSumOfCellValues()
	{
		return sumOfCellValues;
	}


	


}


















