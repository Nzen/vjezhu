/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.model;

import ws.nzen.game.pz.vjezhu.struct.SemiMutableList;
import ws.nzen.game.pz.vjezhu.struct.SemiMutableList_Linked;

/**

*/
public class PossibilityCombo
{

	private SemiMutableList<PossibilityValue> values = new SemiMutableList_Linked<>();


	public PossibilityCombo(
			SemiMutableList<PossibilityValue> suggested
	) {
		values.addAll( suggested );
	}


	public SemiMutableList<PossibilityValue> getValues()
	{
		return values;
	}


}


















