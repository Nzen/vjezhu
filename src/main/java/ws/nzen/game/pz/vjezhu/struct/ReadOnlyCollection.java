/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.struct;

import java.util.stream.Stream;

/**
A collection that only exposes iterator and count methods.
*/
public interface ReadOnlyCollection<Type> extends Iterable<Type>
{


	boolean empty();


	boolean has( Type unknown );


	int len();


	Stream<Type> streamOf();

}


















