/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.struct;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.stream.Stream;

/**

*/
public class SemiMutableSet_Hash<Type> extends HashSet<Type> implements SemiMutableSet<Type>
{


	private static final long serialVersionUID = 1L;


	public SemiMutableSet_Hash(
	) {
		super();
	}


	public SemiMutableSet_Hash(
		Collection<? extends Type> adopt
	) {
		super( adopt );
	}


	public SemiMutableSet_Hash(
		ReadOnlyCollection<Type> values
	) {
		super();
		Iterator<Type> loop = values.iterator();
		while( loop.hasNext() )
		{
			add( loop.next() );
		}
	}


	@Override
	public boolean empty(
	) {
		return isEmpty();
	}


	@Override
	public boolean has(
		Type unknown
	) {
		return contains( unknown );
	}


	@Override
	public int len(
	) {
		return size();
	}


	@Override
	public Stream<Type> streamOf(
	) {
		return stream();
	}


}


















