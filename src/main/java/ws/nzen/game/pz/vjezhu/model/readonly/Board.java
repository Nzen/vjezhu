/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.model.readonly;

import ws.nzen.game.pz.vjezhu.struct.ReadOnlyCollection;
import ws.nzen.game.pz.vjezhu.struct.ReadOnlyMap;

/**

*/
public interface Board
{


	ReadOnlyMap<Coordinate, ? extends Cell> getCells();


	ReadOnlyCollection<? extends Group> getRows();


	ReadOnlyCollection<? extends Group> getColumns();


}


















