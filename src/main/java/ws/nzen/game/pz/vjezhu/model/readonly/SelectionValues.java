
package ws.nzen.game.pz.vjezhu.model.readonly;

import ws.nzen.game.pz.vjezhu.model.Selection.CellElement;

public interface SelectionValues
{


	public Coordinate getCell();


	public CellElement getRegion();


	public int getCombinationIndex();


	public int getPossibleValueIndex();


}
















