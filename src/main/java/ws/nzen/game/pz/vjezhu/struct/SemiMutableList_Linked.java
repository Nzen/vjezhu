/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.struct;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.stream.Stream;

/**
Able to present this as a normal list and a ReadOnlyList.
*/
public class SemiMutableList_Linked<Type> extends LinkedList<Type> implements SemiMutableList<Type>
{


	private static final long serialVersionUID = 1L;


	public SemiMutableList_Linked(
	) {
		super();
	}


	public SemiMutableList_Linked(
		Collection<Type> values
	) {
		super( values );
	}


	public SemiMutableList_Linked(
		ReadOnlyCollection<Type> values
	) {
		super();
		Iterator<Type> loop = values.iterator();
		while( loop.hasNext() )
		{
			add( loop.next() );
		}
	}


	@Override
	public boolean empty(
	) {
		return isEmpty();
	}


	@Override
	public Type getVal(
		int index
	) {
		return get( index );
	}


	@Override
	public boolean has(
		Type unknown
	) {
		return contains( unknown );
	}


	@Override
	public int index(
		Type unknown
	) {
		return indexOf( unknown );
	}


	@Override
	public int lastIndex(
		Type unknown
	) {
		return lastIndexOf( unknown );
	}


	@Override
	public int len(
	) {
		return size();
	}


	@Override
	public Stream<Type> streamOf(
	) {
		return stream();
	}


}


















