
package ws.nzen.game.pz.vjezhu.model;

import ws.nzen.game.pz.vjezhu.model.readonly.Coordinate;
import ws.nzen.game.pz.vjezhu.model.readonly.SelectionValues;

public class Selection implements SelectionValues
{


	public enum CellElement
	{
		ROW_COMBINATIONS, COLUMN_COMBINATIONS, COMMITTED_VALUE, NON_REGION
	};


	private Coordinate coordinate = null;
	private CellElement cellRegion = CellElement.NON_REGION;
	private int combinationIndex = -1;
	private int possibleValueIndex = -1;


	public Selection(
			Coordinate firstCell
	) {
		if ( firstCell == null )
			throw new IllegalArgumentException( "Selection must be a non null cell" );
		coordinate = firstCell;
	}


	public Coordinate getCell(
	) {
		return coordinate;
	}


	public CellElement getRegion(
	) {
		return cellRegion;
	}


	public int getCombinationIndex(
	) {
		return combinationIndex;
	}


	public int getPossibleValueIndex(
	) {
		return possibleValueIndex;
	}


	public void setCell(
			BoardCoordinate where
	) {
		coordinate = where;
	}


	public void setRegion(
			CellElement newRegion
	) {
		cellRegion = newRegion;
	}


	public void setCombinationIndex(
			int newIndex
	) {
		combinationIndex = newIndex;
	}


	public void setPossibleValueIndex(
			int newIndex
	) {
		possibleValueIndex = newIndex;
	}


}
















