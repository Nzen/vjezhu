/* see ../../../../../LICENSE for release details */
package ws.nzen.game.pz.vjezhu.struct;

import java.util.Collection;

/**
A superset of Collection and ReadOnlyCollection.
*/
public interface SemiMutableCollection<Type> extends Collection<Type>, ReadOnlyCollection<Type>
{




}


















